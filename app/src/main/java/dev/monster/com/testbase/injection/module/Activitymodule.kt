package dev.monster.com.testbase.injection.module

import android.app.Activity
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dev.monster.com.testbase.injection.component.ActivityScoped
import dev.monster.com.testbase.ui.MainPresenterModule

/**
 * Created by duc on 5/3/2018.
 */

@Module
abstract class Activitymodule{
    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(MainPresenterModule::class))
    internal abstract fun mainActivity(): MainPresenterModule
}