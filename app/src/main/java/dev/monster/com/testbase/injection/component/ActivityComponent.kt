package dev.monster.com.testbase.injection.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dev.monster.com.testbase.MyApplication
import dev.monster.com.testbase.injection.module.Activitymodule
import dev.monster.com.testbase.injection.module.ApplicationModule
import dev.monster.com.testbase.injection.module.NetworkModule
import dev.monster.com.testbase.ui.MainActivity
import javax.inject.Singleton

/**
 * Created by duc on 5/1/2018.
 */
@Singleton
@Component(modules = arrayOf(NetworkModule::class, Activitymodule::class, ApplicationModule::class))
interface ActivityComponent : AndroidInjector<MyApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): ActivityComponent.Builder

        fun build(): ActivityComponent
    }
}