package dev.monster.com.testbase.injection.module

import dagger.Module
import dagger.Provides
import dev.monster.com.testbase.network.NetworkService
import dev.monster.com.testbase.utils.Constants
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

/**
 * Created by duc on 5/1/2018.
 */
@Module
class NetworkModule {
    @Singleton
    @Provides
    internal fun provideRetrofitInterface(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
    }

    @Provides
    @Singleton
    internal fun providesNetworkService(
            retrofit: Retrofit): NetworkService {
        return retrofit.create(NetworkService::class.java)
    }
}