package dev.monster.com.testbase.base

/**
 * Created by duc on 5/1/2018.
 */
open interface Basepresenter<V> {

    fun attachView(mvpView: V)

    fun detachView()

}