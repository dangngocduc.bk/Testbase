package dev.monster.com.testbase.ui

import dev.monster.com.testbase.base.BaseView
import dev.monster.com.testbase.base.Basepresenter
import dev.monster.com.testbase.models.CityListResponse

/**
 * Created by duc on 5/6/2018.
 */
interface MainContract {
    interface View : BaseView<MainPresenter> {
        fun showData(responst: CityListResponse)
    }

    interface MainPresenter : Basepresenter<View> {
        fun getData()

        override fun attachView(view: View)

        override fun detachView()

    }
}