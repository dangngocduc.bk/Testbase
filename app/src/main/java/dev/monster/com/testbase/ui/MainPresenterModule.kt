package dev.monster.com.testbase.ui

import dagger.Binds
import dagger.Module
import dev.monster.com.testbase.injection.component.ActivityScoped

/**
 * Created by duc on 5/7/2018.
 */
@Module
abstract class MainPresenterModule {
    @ActivityScoped
    @Binds
    internal abstract fun mainPresenter(presenter: MainPresenter): MainContract.MainPresenter
}