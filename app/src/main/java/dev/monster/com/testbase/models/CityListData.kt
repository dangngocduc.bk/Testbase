package dev.monster.com.testbase.models

/**
 * Created by duc on 5/1/2018.
 */
class CityListData(val id: String,
                   val name: String,
                   val description: String,
                   val background: String) {
}