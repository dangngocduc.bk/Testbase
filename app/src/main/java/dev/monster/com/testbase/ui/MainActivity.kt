package dev.monster.com.testbase.ui

import android.os.Bundle
import android.os.PersistableBundle
import dagger.android.support.DaggerAppCompatActivity
import dev.monster.com.testbase.R
import dev.monster.com.testbase.base.BaseActivity
import dev.monster.com.testbase.models.CityListResponse
import javax.inject.Inject


class MainActivity : DaggerAppCompatActivity(), MainContract.View {
    @Inject
    internal var mMainPresenter: MainPresenter? = null

    @Inject
    internal var mPresenter: MainContract.MainPresenter? = null

    override fun showData(responst: CityListResponse) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    override fun onResume() {
        super.onResume()
        mPresenter?.attachView(this)
    }

    override fun onDestroy() {
        mPresenter?.detachView()
        super.onDestroy()
    }
}
