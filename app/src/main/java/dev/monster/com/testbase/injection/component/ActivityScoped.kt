package dev.monster.com.testbase.injection.component

import java.lang.annotation.Documented
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import javax.inject.Scope

/**
 * Created by duc on 5/7/2018.
 */

@Documented
@Scope
@Retention(RetentionPolicy.RUNTIME)
annotation class ActivityScoped
